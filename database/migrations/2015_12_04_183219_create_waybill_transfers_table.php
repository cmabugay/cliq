<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybillTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybill_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_partner_id')->unsigned();
            $table->foreign('shipping_partner_id')->references('id')->on('shipping_partners');
            $table->string('tracking_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('waybill_transfers');
    }
}
