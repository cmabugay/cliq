<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAccountDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_account_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_account_id')->unsigned();
            $table->foreign('user_account_id')->references('id')->on('user_accounts');
            $table->string('email')->unique();
            $table->string('email_change_validation_code')->unique();
            $table->text('facebook_id');
            $table->integer('credit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_account_details');
    }
}
