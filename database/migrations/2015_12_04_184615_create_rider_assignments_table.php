<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiderAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_assignments', function (Blueprint $table) {
            $table->increments('id');      
            $table->integer('rider_id')->unsigned();
            $table->foreign('rider_id')->references('id')->on('riders');
            $table->integer('assignment_location_id')->unsigned();
            $table->foreign('assignment_location_id')->references('id')->on('assignment_locations');
            $table->text('imei');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rider_assignments');
    }
}
