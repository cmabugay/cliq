<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('booking_source_id')->unsigned();
            $table->foreign('booking_source_id')->references('id')->on('booking_sources');
            $table->integer('waybill_type_id')->unsigned();
            $table->foreign('waybill_type_id')->references('id')->on('waybill_types');
            $table->string('pickup_person_name');
            $table->string('pickup_contact_number');
            $table->text('pickup_person_address_line_1');
            $table->text('pickup_person_address_line_2');
            $table->text('pickup_city');
            $table->text('pickup_state_province');
            $table->text('pickup_barangay');
            $table->text('pickup_zip_code');
            $table->text('pickup_country');
            $table->text('pickup_remarks');
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
