<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('assignment_location_id')->unsigned();
            $table->foreign('assignment_location_id')->references('id')->on('assignment_locations');
            $table->string('imei');
            $table->integer('rider_status_id')->unsigned();
            $table->foreign('rider_status_id')->references('id')->on('rider_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('riders');
    }
}
