<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('address_type_id')->unsigned();
            $table->foreign('address_type_id')->references('id')->on('address_types');
            $table->string('name');
            $table->string('contact_person');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->string('city');
            $table->string('state_province');
            $table->string('barangay');
            $table->string('zip_code');
            $table->string('country');
            $table->text('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
