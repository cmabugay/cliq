<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('new_email')->unique();
            $table->string('email_change_validation_code')->unique();
            $table->string('password',60);
            $table->string('facebook_id');
            $table->string('credits');
            $table->integer('user_status_id')->unsigned();
            $table->foreign('user_status_id')->references('id')->on('user_statuses');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
