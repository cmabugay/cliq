<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('pickup_person_name');
            $table->text('pickup_person_address_line_1');
            $table->text('pickup_person_address_line_2');
            $table->text('pickup_city');
            $table->text('pickup_state_province');
            $table->text('pickup_barangay');
            $table->text('pickup_zip_code');
            $table->text('pickup_country');
            $table->text('pickup_remarks');
            $table->integer('booking_source_id')->unsigned();
            $table->foreign('booking_source_id')->references('id')->on('booking_sources');
            $table->string('delivery_person_name');
            $table->text('delivery_person_address_line_1');
            $table->text('delivery_person_address_line_2');
            $table->text('delivery_city');
            $table->text('delivery_state_province');
            $table->text('delivery_barangay');
            $table->text('delivery_zip_code');
            $table->text('delivery_country');
            $table->text('delivery_remarks');
            $table->datetime('date_time');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('waybills');
    }
}
