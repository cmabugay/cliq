<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('waybill_id')->unsigned();
            $table->foreign('waybill_id')->references('id')->on('waybills');
            $table->string('log');
            $table->datetime('date_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipment_logs');
    }
}
