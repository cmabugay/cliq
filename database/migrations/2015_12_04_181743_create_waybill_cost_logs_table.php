<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybillCostLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybill_cost_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('waybill_cost_id')->unsigned();
            $table->foreign('waybill_cost_id')->references('id')->on('waybill_costs');   
            $table->datetime('date_time');     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('waybill_cost_logs');
    }
}
