<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybillInsuranceCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybill_insurance_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('waybill_id')->unsigned();
            $table->foreign('waybill_id')->references('id')->on('waybills');  
            $table->float('amount'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('waybill_insurance_costs');
    }
}
