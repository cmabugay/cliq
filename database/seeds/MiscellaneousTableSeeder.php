<?php

use Illuminate\Database\Seeder;

class MiscellaneousTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = ['id'=>'1','user_type'=> 1];

        $admin_id = DB::table('users')->insertGetId($user);

        $admin_account = ['user_id' => $admin_id,'username' => 'admin', 'password' => bcrypt('admin'), 'status' => 1];

        DB::table('user_accounts')->insert($admin_account);
       
       	$contact_number_types = [
       								['type'=>'Mobile'],
       								['type'=>'Home'],
       								['type'=>'Office/Work']
       							];

       	DB::table('contact_number_types')->insert($contact_number_types);

       	$countries = ['name'=>'Philippines'];

       	DB::table('countries')->insert($countries);

       	$provinces = ['name'=>'IloIlo'];

       	DB::table('state_provinces')->insert($provinces);

       	$city = ['name'=>'Cainta']; 

       	DB::table('cities')->insert($city);

       	$booking_sources = ['name'=>'Website'];

       	DB::table('booking_sources')->insert($booking_sources);

       	$waybill_types = ['type'=>'Type 1'];

       	DB::table('waybill_types')->insert($waybill_types);


        $rider_users  =  ['id'=>'2','user_type'=> 3];

        $user_id = DB::table('users')->insertGetId($rider_users);


        $user_accounts = ['user_id' => $user_id,'username' => 'rider1', 'password' => bcrypt('rider1'), 'status' => 1];



        $user_account_id = DB::table('user_accounts')->insertGetId($user_accounts);


        $rider = ['user_account_id' => $user_account_id];


        $rider_id = DB::table('riders')->insertGetId($rider);


        $rider_details = ['rider_id' => $rider_id, 'firstname' => 'mask', 'middlename' => 'rider', 'lastname' => 'black'];

       
        $rider_id = DB::table('rider_details')->insert($rider_details);


        $assignment_location = ['location' => 'IloIlo'];


        $assignment_location_id = DB::table('assignment_locations')->insertGetId($assignment_location);

        $rider_assignment = ['rider_id' => $rider_id, 'assignment_location_id' => $assignment_location_id, 'imei' => '07220971313'];


        DB::table('rider_assignments')->insert($rider_assignment);




    }
}
