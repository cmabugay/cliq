<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateProvince extends Model
{
	protected $table = 'state_provinces';

    public $timestamps  = false;

    public $fillable = ['name'];
}
