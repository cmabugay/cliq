<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'customer_address';

	public $timestamps  = false;

	public $fillable = ['address_line_1','address_line_1','city','state_province','barangay','zip_code','country'];
}
