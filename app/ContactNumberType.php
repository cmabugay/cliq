<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactNumberType extends Model
{
	protected $table = 'contact_number_types';

    public $timestamps  = false;

    public $fillable = ['type'];
}
