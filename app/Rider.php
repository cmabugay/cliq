<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
    public $table = 'riders';

    public $timestamps = false;

    public function detail()
    {
    	return $this->hasOne('App\RiderDetail');
    }
}
