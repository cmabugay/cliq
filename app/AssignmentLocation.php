<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentLocation extends Model
{
    public $table = 'assignment_locations';

    public $timestamps = false;

    public $fillable = ['location'];

    
}
