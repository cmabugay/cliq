<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderDetail extends Model
{
    public $table = 'rider_details';

    public $timestamps = 'false';

    public $fillable = [
    					'firstname',
    					'middlename',
    					'lastname'
    					];
}
