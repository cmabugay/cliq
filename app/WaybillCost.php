<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaybillCost extends Model
{
    protected $table = 'waybill_costs';

    public $timestamps  = false;

	public $fillable = ['amount'];
}
