<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingSource extends Model
{
	protected $table = 'booking_sources';

    public $timestamps  = false;

	public $fillable = ['name'];
}
