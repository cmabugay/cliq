<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderAssignment extends Model
{    
	public $table = 'rider_assignments';

  	public $timestamps = false;

    public $fillable = ['imei'];
    
}
