<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentItem extends Model
{
    public $table = 'shipment_item';

    public $timestamps = false;

    public $fillable = ['description'];

    
}
