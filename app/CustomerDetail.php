<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDetail extends Model
{
    public $table = 'customer_details';

    public $timestamps = false;

    public $fillable = ['firstname','middlename', 'lastname','birthday'];
}
