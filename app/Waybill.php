<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waybill extends Model
{
	public $table = 'waybills';

    public $timestamps = false;

    public $fillable = [
    					'pickup_person_name',
    					'pickup_person_address_line_1',
    					'pickup_person_address_line_2',
    					'pickup_city',
    					'pickup_state_province',
    					'pickup_barangay',
    					'pickup_zip_code',
    					'pickup_country',
    					'pickup_remarks',
    					'delivery_person_name',
    					'delivery_person_address_line_1',
    					'delivery_person_address_line_2',
    					'delivery_city',
    					'delivery_state_province',
    					'delivery_barangay',
    					'delivery_zip_code',
    					'delivery_country',
    					'delivery_remarks'
    					];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }
}
