<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderBookingAssignment extends Model
{
    public $timestamps = false;
}
