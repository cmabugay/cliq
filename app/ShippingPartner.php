<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingPartner extends Model
{
    protected $table = 'shipping_partners';

    public $timestamps  = false;

	public $fillable = ['name','url','status'];
}
