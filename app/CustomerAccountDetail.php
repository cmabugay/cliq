<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAccountDetail extends Model
{
    public $table = 'customer_account_details';

    public $timestamps = false;

    public $fillable = ['new_email','email_change_validation_code', 'facebook_id','credit'];

}
