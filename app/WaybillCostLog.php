<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaybillCostLog extends Model
{
    protected $table = 'waybill_cost_logs';

    public $timestamps  = false;

	public $fillable = ['status','date_time'];
}
