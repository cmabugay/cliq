<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentLog extends Model
{
    protected $table = 'shipment_logs';

    public $timestamps  = false;

	public $fillable = ['log','date_time'];
}
