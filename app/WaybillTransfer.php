<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaybillTransfer extends Model
{
    protected $table = 'waybill_transfers';

    public $timestamps  = false;

	public $fillable = ['tracking_number'];
}
