<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\StateProvince;
use App\City;
use App\Country;
use App\Rider;
use App\Booking;
use Auth;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function provinces()
    {
        $provinces = StateProvince::all();

        return $provinces->toJson();
    }

    public function city()
    {
        $city = City::all();

        return $city->toJson();
    }

    public function country()
    {
        $country = Country::all();

        return $country->toJson();
    }


    public function riders()
    {
        $riders = Rider::with('detail')->get();

        return $riders->toJson();
    }

    public function bookings()
    {
        $bookings = Booking::where('status','2')->get();
        $new_bookings = [];

        foreach($bookings as $booking) 
        {
            $data = [];
            $data = ['id'=> $booking->id, 'name' => $booking->id];
            array_push($new_bookings, $data);
        }
        return json_encode($new_bookings);
    }

    public function logout() 
    {
        Auth::logout();
        return redirect()->route('admin-login');
    }
}