<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AdminBookingUserRequest;
use App\Http\Requests\AdminBookingRequest;

use App\User;
use App\UserAccount;
use App\CustomerDetail;
use App\CustomerAccountDetail;
use App\CustomerContactNumber;
use App\Booking;
use App\RiderBookingAssignment;
use App\Rider;  
use App\Waybill;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //status_id : 1 - pending, 2 - assigned to rider, 3 - dispatch, 4 - cancel

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bookings = Booking::where('status',1)->get();

        $data['bookings'] = Booking::where('status',1)->get();
        // return view('admin.booking.index',)->with('bookings',$bookings);
        return view('admin.booking.index',$data);
    }   


    /*
     * 
     * 7 Dec 2015 Update
     * Author : Kenneth Sunday
     * Time : 3:41 PM
     */
    public function pending(){
        $data['bookings'] = Booking::where('status',1)->get();
        return view('admin.booking.pending',$data);

    }

    public function dispatch_booking(){
        $data['bookings'] = Booking::where('status',2)->get();
        return view('admin.booking.dispatch',$data);
    }

    public function completed(){
        $data['bookings'] = Booking::where('status',3)->get();
        return view('admin.booking.completed',$data);
    }

    public function cancelled(){
        $data['bookings'] = Booking::where('status',4)->get();
        return view('admin.booking.cancelled',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.booking.create.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNew()
    {
        return view('admin.booking.create.new');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOld()
    {
        $users = User::where('user_type',2)->get();

        return view('admin.booking.create.old')->with('users', $users);
    }

    public function createOldWithDetail($id)
    {
        $user = User::findOrFail($id);

        return view('admin.booking.create.detail')->with('user', $user);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminBookingRequest $request)
    {
        if($request->customer_type == "new") 
        {

        $user = new User;

        $user->user_type = 2;

        $user->save();

        $user_id = $user->id;

        if($request->email != '') 
        {
            $user_account = new UserAccount;

            $user_account->user_id = $user_id;

            $user_account->username = $request->email;

            $user_account->password = bcrypt($request->email);

            $user_account->status = 1;

            $user_account->save();

            $user_account_id = $user_account->id;

            $account_details = new CustomerAccountDetail;

            $account_details->user_account_id = $user_account_id;

            $account_details->save();


        }

        
        $customer_details = new CustomerDetail;

        $customer_details->user_id = $user_id;

        $customer_details->firstname = $request->firstname;

        $customer_details->middlename = $request->middlename;

        $customer_details->lastname = $request->lastname;

        $customer_details->save();

        $customer_contact_number = new CustomerContactNumber;

        $customer_contact_number->user_id = $user_id;

        $customer_contact_number->contact_number = $request->contact_number;

        $customer_contact_number->contact_number_type_id = 1;

        $customer_contact_number->save();



        } else {

            $user_id = $request->user_id;
        }


         $booking = new Booking;

        $booking->user_id = $user_id;

        $booking->pickup_person_name = $request->contact_person_name;

        $booking->pickup_contact_number = $request->booking_contact_number;

        $booking->pickup_state_province = $request->region_province;

        $booking->pickup_person_address_line_1 = $request->street_address_1;

        $booking->pickup_city = $request->city_municipality;

        $booking->pickup_person_address_line_2 = $request->street_address_2;

        $booking->pickup_barangay = $request->barangay;

        $booking->pickup_zip_code = $request->zip_code;

        $booking->pickup_country = $request->country;

        $booking->pickup_remarks = $request->remarks;

        
        $booking->pickup_remarks = $request->remarks;

        $booking->status = 1;


        $booking->booking_source_id = 1;

        $booking->waybill_type_id = 1;

        $booking->save();

        $booking_id = $booking->id;


        

        return redirect()->route('admin-booking-confirmation',[$booking_id]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirmation($id)
    {

        // $booking = Booking::findOrFail($id)->where('status',"1")->first();
        $data['booking'] = Booking::where('id',$id)->first();
        return view('admin.booking.confirm',$data);
    }


    public function setRider(Request $request)
    {
        
        $booking_id = $request->booking_id;
        $rider_id = $request->rider_id;


        $rider_booking = new RiderBookingAssignment;

        $rider_booking->rider_id = $rider_id;
        $rider_booking->booking_id = $booking_id;

        $rider_booking->save();

        $booking = Booking::findOrFail($booking_id);

        $booking->status = 2;
        $booking->save();
        /*
        $waybill = new Waybill;

        $waybill->booking_id = $booking_id;
        $waybill->user_id = $booking->user_id;
        $waybill->pickup_person_name = $booking->pickup_person_name;
        $waybill->pickup_person_address_line_1 = $booking->pickup_person_address_line_1;
        $waybill->pickup_person_address_line_1 = $booking->pickup_person_address_line_1;
        $waybill->pickup_city = $booking->pickup_city;
        $waybill->pickup_state_province = $booking->pickup_state_province;
        $waybill->pickup_barangay = $booking->pickup_barangay;
        $waybill->zip_code = $booking->zip_code;
        $waybill->country = $booking->country;
        $waybill->remarks = $booking->remarks;
        $waybill->booking_source_id = $booking->booking_source_id;

        $waybill->save();
        */

        return "done";
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {



        return view('admin.booking.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.booking.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
