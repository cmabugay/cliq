<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AdminWaybillRequest;

use App\Booking;
use App\Waybill;
class WaybillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.waybill.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function build(Request $request)
    {
        return redirect()->route('admin-waybill-add',$request->booking_id);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($booking_id)
    {

        $booking = Booking::findOrFail($booking_id);

        return view('admin.waybill.add')->with('booking',$booking);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminWaybillRequest $request)
    {
         $waybill = new Waybill;

        $waybill->booking_id = $request->booking_id;
        $waybill->user_id = $request->user_id;
        $waybill->pickup_person_name = $request->pickup_person_name;
        $waybill->pickup_person_address_line_1 = $request->pickup_address_line_1;
        $waybill->pickup_person_address_line_1 = $request->pickup_address_line_1;
        $waybill->pickup_city = $request->pickup_city;
        $waybill->pickup_state_province = $request->pickup_state_province;
        $waybill->pickup_barangay = $request->pickup_barangay;
        $waybill->pickup_zip_code = $request->pickup_zip_code;
        $waybill->pickup_country = $request->pickup_country;
        $waybill->pickup_remarks = $request->pickup_remarks;
        $waybill->booking_source_id = $request->booking_source_id;

        $waybill->delivery_person_name = $request->delivery_person_name;
        $waybill->delivery_person_address_line_1 = $request->delivery_address_line_1;
        $waybill->delivery_person_address_line_1 = $request->delivery_address_line_1;
        $waybill->delivery_city = $request->delivery_city;
        $waybill->delivery_state_province = $request->delivery_state_province;
        $waybill->delivery_barangay = $request->delivery_barangay;
        $waybill->delivery_zip_code = $request->delivery_zip_code;
        $waybill->delivery_country = $request->delivery_country;
        $waybill->delivery_remarks = $request->delivery_remarks;

        $waybill->save();

        $waybill_id = $waybill->id;

        return redirect()->route('admin-waybill-confirmation',[$waybill_id]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirmation($id)
    {

        $waybill = Waybill::findOrFail($id);

        //dd($waybill->booking->user);

        return view('admin.waybill.confirmation')->with('waybill', $waybill);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
