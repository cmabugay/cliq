<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('provinces', 'AdminController@provinces')->name('provinces');
Route::get('city', 'AdminController@city')->name('city');
Route::get('country', 'AdminController@country')->name('country');
Route::get('riders', 'AdminController@riders')->name('riders');
Route::get('bookings', 'AdminController@bookings')->name('bookings');


/*
 * 
 * 7 Dec 2015 Update
 * Author : Kenneth Sunday
 * Time : 3:41 PM
 */
$router->group(['prefix' => "admin"],function(){
	$this->group(['prefix' => "booking"],function(){
		$this->get('pending',['as' => "admin.booking.pending",'uses' => "BookingController@pending"]);
		$this->get('completed',['as' => "admin.booking.completed",'uses' => "BookingController@completed"]);
		$this->get('cancelled',['as' => "admin.booking.cancelled",'uses' => "BookingController@cancelled"]);
		$this->get('for-dispatch',['as' => "admin.booking.dispatch",'uses' => "BookingController@dispatch_booking"]);
	});
});

$router->group(['as' => 'admin-'], function () {

    Route::get('login', 'Auth\AuthController@login')->name('login');
    Route::post('login', 'Auth\AuthController@logUser')->name('login');
    Route::get('logout', 'AdminController@logout')->name('logout');

    //Route::get('register', 'Auth\AuthController@register')->name('register');


	    
    	Route::get('admin/dashboard', 'AdminController@index')->name('index');    	


	   $this->group(['as' => 'booking-'], function () {

	    	// Booking Listings
		    $this->get('admin/booking', 'BookingController@index')->name('index');




		    $this->post('admin/booking/set/rider', 'BookingController@setRider')->name('set-rider');

		    // Create booking New or Old
		    $this->get('admin/booking/create', 'BookingController@create')->name('create');

		    // Create new booking (user)
		    $this->get('admin/booking/create/new', 'BookingController@createNew')->name('create-new');

		    // Create old booking
		    $this->get('admin/booking/create/old', 'BookingController@createOld')->name('create-old');

		    $this->get('admin/booking/create/old/{id}', 'BookingController@createOldWithDetail')->name('create-old-detail');

		    // Create view booking
		    $this->get('admin/booking/show/{id}', 'BookingController@show')->name('show');

		    // Create store booking (user)
		    $this->post('admin/booking/store', 'BookingController@store')->name('store');

		     $this->get('admin/booking/confirmation/{id}', 'BookingController@confirmation')->name('confirmation');


		    // Create edit booking
		    $this->get('admin/booking/edit/{id}', 'BookingController@edit')->name('edit');

		    // Create delete booking
		    $this->get('admin/booking/destroy/{id}', 'BookingController@edit')->name('destroy');

		    

		});

		
		
	

		Route::group(['as' => 'waybill-'], function () {

			Route::get('admin/waybill/create', 'WaybillController@create')->name('create');

			Route::post('admin/waybill/build', 'WaybillController@build')->name('build');

			Route::get('admin/waybill/add/{booking_id}', 'WaybillController@add')->name('add');

			Route::post('admin/waybill/store', 'WaybillController@store')->name('store');

			Route::get('admin/waybill/confirmation/{id}', 'WaybillController@confirmation')->name('confirmation');

		});
	
}); 		




