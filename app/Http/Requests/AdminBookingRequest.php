<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->input('customer_type') == "new")
        {
            return [
                'firstname' => 'required',
                'lastname' => 'required',
                //'middlename' => 'required',
                'contact_number' => 'required',
                'email' => 'email',
                ////'password' => 'min:8',
                'confirm_password' => 'same:password',
                'contact_person_name' => 'required',
                'booking_contact_number' => 'required',
                'region_province' => 'required',
                'street_address_1' => 'required',
                'city_municipality' => 'required',
                //'barangay' => 'required',
                'zip_code' => 'required',
                'country' => 'required',
                'remarks' => '',
            ];

        } else {
            return [
                'contact_person_name' => 'required',
                'booking_contact_number' => 'required',
                'region_province' => 'required',
                'street_address_1' => 'required',
                'city_municipality' => 'required',
                'barangay' => 'required',
                'zip_code' => 'required',
                'remarks' => '',
            ];
        }
        
    }
}
