<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminWaybillRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delivery_person_name' => 'required',
            'delivery_state_province' => 'required',
            'delivery_address_line_1' => 'required',
            'delivery_city' => 'required',
            'delivery_barangay' => 'required',
            'delivery_zip_code' => 'required',
            'delivery_country' => 'required',
            'remarks' => '',
        ];
    }
}
