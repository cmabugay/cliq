<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaybillType extends Model
{
    protected $table = 'waybill_types';

    public $timestamps  = false;

	public $fillable = ['type'];

	
}
