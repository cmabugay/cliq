<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    public $timestamp = false;

    public $fillable = ['status'];
}
