<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchStatus extends Model
{
    public $timestamp = false;

    public $fillable = ['status'];
}
