<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    public $timestamp = false;

    public $fillable = ['type'];
}
