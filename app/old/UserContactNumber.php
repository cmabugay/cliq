<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContactNumber extends Model
{
    public $timestamp = false;

    public $fillable = ['contact_numebr'];
}
