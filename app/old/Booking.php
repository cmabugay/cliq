<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public $timestamps = false;

    public $fillable = [
    					'pickup_person_name',
    					'pickup_person_address_line_1',
    					'pickup_person_address_line_2',
    					'pickup_city',
    					'pickup_state_province',
    					'pickup_barangay',
    					'pickup_zip_code',
    					'pickup_country',
    					'pickup_remarks'
    					];
}
