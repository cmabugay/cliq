<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingSource extends Model
{
    public $timestamp = false;

    public $fillable = ['source'];
}
