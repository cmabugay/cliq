<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentLocation extends Model
{
    public $timestamp = false;

    public $fillable = ['location'];
}
