<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCreditsHistory extends Model
{
    public $timestamp = false;

    public $fillable = ['amount','date_time'];
}
