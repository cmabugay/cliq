<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderStatus extends Model
{
    public $timestamp = false;

    public $fillable = ['status'];
}
