<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactNumberType extends Model
{
    public $timestamp = false;

    public $fillable = ['type'];
}
