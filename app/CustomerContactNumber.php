<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerContactNumber extends Model
{
    public $table = 'customer_contact_numbers';

    public $timestamps = false;

    public $fillable = ['contact_number'];
}
