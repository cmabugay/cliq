<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;

    public $fillable = [
    						'contact_person',
    						'address_line_1',
    						'address_line_2',
    						'city',
    						'state_province',
    						'barangay',
    						'zip_code',
    						'country',
    						'notes'
    				   ];
}
