<html>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cliq | @yield('title')</title>
        <meta name="description" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="{!! asset('admin/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('admin/css/bootstrap.css') !!}">

        @yield('added-styles')

        <link rel="stylesheet" href="{!! asset('admin/css/main.css') !!}">   

        <script src="{!! asset('admin/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') !!}"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          

        </div>
      </div>
    </nav> -->

    @section('sidebar')
    <div class="main-menu col-md-2">
        <div class="logo">
            <img src="{!! asset('admin/img/logo.png') !!}" alt="">
        </div>
        <div class="profile">
            <img src="{!! asset('admin/img/profile.jpg') !!}" alt="">
            <h4>{!! Auth::user()->username !!}</h4>
        </div>
        <ul id="accordion" class="accordion">
            <li class="open">
                <div class="link"><i class="fa fa-home"></i>Dashboard<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu" style="display: block;">
                  <li><a href="{!! route('admin-index') !!}">All</a></li>
                  <li><a href="#">Reports</a></li>
                </ul>
            </li>
            <li class="open">
                <div class="link"><i class="fa fa-calendar"></i>Booking<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu" style="display: block;">
                  <li ><a href="{!! route('admin-booking-create') !!}">Create Booking</a></li>
                  <li><a href="{{route('admin.booking.pending')}}">Pending Booking</a></li>
                  <li><a href="{{route('admin.booking.dispatch')}}">Dispatched Booking</a></li>
                  <li><a href="{{route('admin.booking.completed')}}">Completed Booking</a></li>
                  <li><a href="{{route('admin.booking.cancelled')}}">Cancelled Booking</a></li>
                </ul>
            </li>
            <li class="open" >
                <div class="link"><i class="fa fa-calendar"></i>Waybill<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu" style="display: block;">
                  <li><a href="{!! route('admin-waybill-create') !!}">Add</a></li>
                </ul>
            </li>
            <li class="open">
                <div class="link"><i class="fa fa-user"></i>User<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu" style="display: block;">
                  <li><a href="{!! route('admin-logout') !!}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    @show

    <div class="main-body col-md-10">
        @yield('content')

    </div>
    <div class="clearfix"></div>
    @section('footer')
    <footer>
        <p>&copy; Cliqnship. All Rights Reserved 2015</p>
    </footer>
    @show
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
        <script src="http://code.jquery.com/jquery-latest.js"></script>

        <script src="{!! asset('admin/js/vendor/bootstrap.min.js') !!}"></script>

        @yield('added-scripts')

        <script src="{!! asset('admin/js/main.js') !!}"></script>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            // (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            // function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            // e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            // e.src='//www.google-analytics.com/analytics.js';
            // r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            // ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
