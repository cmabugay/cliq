@extends('admin.layout')

@section('title', 'Waybill')


@section('added-styles')
    
@endsection

@section('added-scripts')
   <script type="text/javascript"  src="{!! asset('admin/js/typehead.js') !!}"></script>
   <script type="text/javascript">
   		

		$.get('../../../../provinces', function(data){
        $("#delivery_state_province").typeahead({ source:data });
          },'json');

          $.get('../../../../city', function(data){
              $("#delivery_city").typeahead({ source:data });
          },'json');

          $.get('../../../../country', function(data){
              $("#delivery_country").typeahead({ source:data });
          },'json');
   </script>
@endsection


@section('content')
<div class="dashboard-head">
	<div class="pull-left">
	    <h1>Waybill Details</h1>
	</div>
	<div class="clearfix"></div>
	<p>Add waybill details on booked items to deliver.</p>
</div>
<hr>
<div class="col-md-12" >
	   
    	<form class="col-md-8" method="POST" action="{!!route('admin-waybill-store')!!}">
  		{!! csrf_field() !!}
  		<input type="hidden" name="user_id" value="{!! $booking->user_id !!}"/>
  		<input type="hidden" name="booking_source_id" value="{!! $booking->booking_source_id !!}"/>
  		<input type="hidden" name="booking_id" value="{!! $booking->id !!}"/>
		        <div class="col-md-12">
		        	<h3>Waybill Details</h3>
		        	  <div class="col-md-6">
		        	  		<h4>Pickup Details</h4>
							<div class="form-group col-md-12">
							    <label for="pickup_person_name"> Person Name: </label>
							    <input type="text" class="form-control" id="bookings"  name="pickup_person_name" placeholder="" value="{!! $booking->pickup_person_name !!}">
							</div> 
					 		<div class="form-group col-md-12">
							    <label for="pickup_address_line_1"> Address line 1: </label>
							    <input type="text" class="form-control" id="pickup_address_line_1"  name="pickup_address_line_1" placeholder="" value="{!! $booking->pickup_address_line_1 !!}">
							</div>   
							<div class="form-group col-md-12">
							    <label for="pickup_address_line_2"> Address line 2: </label>
							    <input type="text" class="form-control" id="pickup_address_line_2"  name="pickup_address_line_2" placeholder="" value="{!! $booking->pickup_address_line_2 !!}">
							</div> 
							<div class="form-group col-md-12">
							    <label for="pickup_state_province	"> Barangay: </label>
							    <input type="text" class="form-control" id="pickup_barangay"  name="pickup_barangay" placeholder="" value="{!! $booking->pickup_barangay !!}">
							</div> 
							<div class="form-group col-md-12">
							    <label for="pickup_city"> City: </label>
							    <input type="text" class="form-control" id="pickup_city"  name="pickup_city" placeholder="" value="{!! $booking->pickup_city !!}">
							</div>  
							<div class="form-group col-md-12">
							    <label for="pickup_state_province"> State / Province: </label>
							    <input type="text" class="form-control" id="pickup_state_province"  name="pickup_state_province" placeholder="" value="{!! $booking->pickup_state_province !!}">
							</div> 
							<div class="form-group col-md-12">
							    <label for="pickup_zip_code"> Zip Code: </label>
							    <input type="text" class="form-control" id="ppickup_country"  name="pickup_zip_code" placeholder="" value="{!! $booking->pickup_zip_code!!}">
							</div>
							<div class="form-group col-md-12">
							    <label for="pickup_country"> Country: </label>
							    <input type="text" class="form-control" id="pickup_country"  name="pickup_country" placeholder="" value="{!! $booking->pickup_country !!}">
							</div>
							<div class="form-group col-md-12">
							    <label for="pickup_country"> Remarks: </label>
							    <input type="text" class="form-control" id="pickup_remarks"  name="pickup_remarks" placeholder="" value="{!! $booking->pickup_remarks !!}">
							</div>  
		        	  		</div>
		        	  <div class="col-md-6">
		        	  		<h4>Delivery Details</h4>
		        	  		
					 		  <div class="form-group col-md-12 {!! $errors->first('delivery_person_name') ? 'has-error' : '' !!}">
							    <label for="delivery_person_name"> Person Name: <span class="required">*</span></label>
							    <input type="text" class="form-control" id="delivery_person_name"  name="delivery_person_name" placeholder="" value="{!! old('delivery_person_name') !!}">
							    @if( $errors->first('delivery_person_name') )
							  		{!!  $errors->first('delivery_person_name') !!}
							  	@endif
								</div> 
						 		<div class="form-group col-md-12 {!! $errors->first('delivery_address_line_1') ? 'has-error' : '' !!}">
								    <label for="delivery_address_line_1"> Address line 1: <span class="required">*</span></label>
								    <input type="text" class="form-control" id="delivery_address_line_1"  name="delivery_address_line_1" placeholder="" value="{!! old('delivery_address_line_1')  !!}">
								@if( $errors->first('delivery_address_line_1') )
							  		{!!  $errors->first('delivery_address_line_1') !!}
							  	@endif
								</div>   
								<div class="form-group col-md-12 {!! $errors->first('delivery_address_line_2') ? 'has-error' : '' !!}">
								    <label for="delivery_address_line_2"> Address line 2: </label>
								    <input type="text" class="form-control" id="delivery_address_line_2"  name="delivery_address_line_2" placeholder="" value="{!! old('delivery_address_line_2')  !!}">
								@if( $errors->first('delivery_address_line_2') )
							  		{!!  $errors->first('delivery_address_line_2') !!}
							  	@endif
								</div> 
								<div class="form-group col-md-12 {!! $errors->first('delivery_barangay') ? 'has-error' : '' !!}">
								    <label for="delivery_state_province	"> Barangay: </label>
								    <input type="text" class="form-control" id="delivery_barangay"  name="delivery_barangay" placeholder="" value="{!! old('delivery_barangay')  !!}">
								@if( $errors->first('delivery_barangay') )
							  		{!!  $errors->first('delivery_barangay') !!}
							  	@endif
								</div> 
								<div class="form-group col-md-12 {!! $errors->first('delivery_city') ? 'has-error' : '' !!}">
								    <label for="delivery_city"> City: <span class="required">*</span></label>
								    <input type="text" class="form-control" id="delivery_city"  name="delivery_city" placeholder="" value="{!! old('delivery_city')  !!}">
								@if( $errors->first('delivery_city') )
							  		{!!  $errors->first('delivery_city') !!}
							  	@endif
								</div>  
								<div class="form-group col-md-12 {!! $errors->first('delivery_state_province') ? 'has-error' : '' !!}">
								    <label for="delivery_state_province"> State / Province: <span class="required">*</span></label>
								    <input type="text" class="form-control" id="delivery_state_province"  name="delivery_state_province" placeholder="" value="{!! old('delivery_state_province')  !!}">
								@if( $errors->first('delivery_state_province') )
							  		{!!  $errors->first('delivery_state_province') !!}
							  	@endif
								</div>
								<div class="form-group col-md-12 {!! $errors->first('delivery_zip_code') ? 'has-error' : '' !!}">
								    <label for="delivery_zip_code"> Zip Code: <span class="required">*</span></label>
								    <input type="text" class="form-control" id="pdelivery_country"  name="delivery_zip_code" placeholder="" value="{!! old('delivery_zip_code') !!}">
								@if( $errors->first('delivery_zip_code') )
							  		{!!  $errors->first('delivery_zip_code') !!}
							  	@endif
								</div>
								<div class="form-group col-md-12 {!! $errors->first('delivery_country') ? 'has-error' : '' !!}">
								    <label for="delivery_country"> Country: <span class="required">*</span></label>
								    <input type="text" class="form-control" id="delivery_country"  name="delivery_country" placeholder="" value="{!! old('delivery_country')  !!}">
								@if( $errors->first('delivery_country') )
							  		{!!  $errors->first('delivery_country') !!}
							  	@endif
								</div>
								<div class="form-group col-md-12 {!! $errors->first('delivery_remarks') ? 'has-error' : '' !!}">
								    <label for="delivery_country"> Remarks: </label>
								    <input type="text" class="form-control" id="delivery_remarks"  name="delivery_remarks" placeholder="" value="{!! old('delivery_remarks')  !!}">
								@if( $errors->first('delivery_remarks') )
							  		{!!  $errors->first('delivery_remarks') !!}
							  	@endif
								</div>  



		        	  		</div>
		        	  </div>
		        	<button type="submit" class="btn btn-primary">Add Waybill</button>	
		        </div> 	
	    </div>
		</form>
	</div>


    

@endsection