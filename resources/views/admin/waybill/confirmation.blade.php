@extends('admin.layout')

@section('title', 'Booking')


@section('added-styles')
      <link rel="stylesheet" type="text/css" href="{!! asset('admin/css/dataTables.bootstrap.css') !!}">
@endsection

@section('added-scripts')
    
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Waybill Confirmation</h1>
        </div>
        
        <div class="clearfix"></div>
       
    </div>
    <hr>
    
    <div class="clearfix"></div>



    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Booking ID:</td>
                    <td>{!! $waybill->booking->id !!}</td>
                  </tr>
                  <tr>
                  <td>Customer Name:</td>
                  <td>{!! $waybill->booking->user->detail->firstname. ' ' . $waybill->booking->user->detail->middlename .  ' ' . $waybill->booking->user->detail->lastname !!}</td>
                  </td>
                  <tr>
                    <td>Contact Number:</td>
                    <td>{!! $waybill->booking->user->contact->contact_number !!}</td>
                  </tr>
                  <tr>
                    <td colspan="2">Pickup Details:</td> 
                  </tr>
                  <tr>
                    <td>Pickup Person Name:</td>
                    <td>{!! $waybill->pickup_person_name !!}</td>
                  </tr>
                   <tr>
                    <td>Pickup Person Contact Number:</td>
                    <td>{!! $waybill->pickup_contact_number !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Address 1:</td>
                    <td>{!! $waybill->pickup_person_address_line_1 !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Address 2:</td>
                    <td>{!! $waybill->pickup_person_address_line_2 !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Barangay:</td>
                    <td>{!! $waybill->pickup_barangay !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup City:</td>
                    <td>{!! $waybill->pickup_city !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup State / Province:</td>
                    <td>{!! $waybill->pickup_state_province !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Country:</td>
                    <td>{!! $waybill->pickup_country !!}</td>
                  </tr>

                  <tr>
                    <td colspan="2">Delivery Details:</td> 
                  </tr>
                  <tr>
                    <td>Pickup Person Name:</td>
                    <td>{!! $waybill->pickup_person_name !!}</td>
                  </tr>
                   <tr>
                    <td>Delivery Person Contact Number:</td>
                    <td>{!! $waybill->delivery_contact_number !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery Address 1:</td>
                    <td>{!! $waybill->delivery_person_address_line_1 !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery Address 2:</td>
                    <td>{!! $waybill->delivery_person_address_line_2 !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery Barangay:</td>
                    <td>{!! $waybill->delivery_barangay !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery City:</td>
                    <td>{!! $waybill->delivery_city !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery State / Province:</td>
                    <td>{!! $waybill->delivery_state_province !!}</td>
                  </tr>
                  <tr>
                    <td>Delivery Country:</td>
                    <td>{!! $waybill->delivery_country !!}</td>
                  </tr>

                </tbody>
            </table>
        </div>
        <br />
        <div class="col-md-6">
          <div class="row">
              <div class="col-md-6">
                  <a href="{!! route('admin-waybill-create') !!}" class="btn btn-primary btn-block btn-lg">New Waybill</a>
              </div>
              <div class="col-md-6">
                  <a href="{!! route('admin-waybill-add',$waybill->booking->id) !!}" class="btn btn-primary btn-block btn-lg">Add Waybill in this booking</a>
              </div>
          </div>
        </div>
    </div>

@endsection