@extends('admin.layout')

@section('title', 'Waybill')


@section('added-styles')
    
@endsection

@section('added-scripts')
   <script type="text/javascript"  src="{!! asset('admin/js/typehead.js') !!}"></script>
   <script type="text/javascript">
   		$.get('../../../bookings', function(data){

		    $("#bookings").typeahead({ source:data });
		},'json');
   </script>
@endsection


@section('content')
<div class="dashboard-head">
	<div class="pull-left">
	    <h1>Add Waybill</h1>
	</div>
	<div class="clearfix"></div>
	<p>Add waybill on booked items to deliver.</p>
</div>
<hr>
<div class="col-md-12" >
	   
    	<form class="col-md-8" method="POST" action="{!!route('admin-waybill-build')!!}">
  		{!! csrf_field() !!}
  		
		        <div class="col-md-12">
		        	<h3>Booking Detail</h3>
					  <div class="form-group col-md-12 {!! $errors->first('booking_id') ? 'has-error' : '' !!}">
					    <label for="bookings"> Booking ID: <span class="required">*</span></label>
					    <input type="text" class="form-control" id="bookings"  name="booking_id" placeholder="" value="{!! old('booking_id') !!}">
					  	@if( $errors->first('booking_id') )
					  		{!!  $errors->first('booking_id') !!}
					  	@endif
					  </div> 
					  <button type="submit" class="btn btn-primary">Add Waybill</button>
		        </div> 	
	    </div>
		</form>
	</div>


    

@endsection