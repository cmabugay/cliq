@extends('admin.layout')

@section('title', 'Dashboard')


@section('added-styles')
      <link rel="stylesheet" href="{!! asset('admin/css/jquery.bxslider.css') !!}">
@endsection

@section('added-scripts')
    <script src="{!! asset('admin/js/jquery.bxslider.js') !!}"></script>
@endsection


@section('content')

    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Dashboard</h1>
        </div>
        <div class="pull-right">
            <h4><strong>Local Time:</strong> Nov 12, 2015 - 05:04 pm</h4>
        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Slider -->
    <ul class="bxslider">
      <li>
        <div class="bx-content">
            <h4>New Booking</h4>
            <h3><span>002</span> today</h3>
        </div>
        <div class="bx-icon">
            <i class="fa fa-calendar"></i>
        </div>

      </li>
      <li>
        <div class="bx-content">
            <h4>New Shipments</h4>
            <h3><span>029</span> today</h3>
        </div>
        <div class="bx-icon">
            <i class="fa fa-plane"></i>
        </div>

      </li>
      <li>
        <div class="bx-content">
            <h4>New Customers</h4>
            <h3><span>012</span> today</h3>
        </div>
        <div class="bx-icon">
            <i class="fa fa-users"></i>
        </div>

      </li>
      <li>
        <div class="bx-content">
            <h4>New Support Tickets</h4>
            <h3><span>002</span> today</h3>
        </div>
        <div class="bx-icon">
            <i class="fa fa-calendar"></i>
        </div>

      </li>

      <li>
        <div class="bx-content">
            <h4>New Support Tickets</h4>
            <h3><span>002</span> today</h3>
        </div>
        <div class="bx-icon">
            <i class="fa fa-calendar"></i>
        </div>

      </li>
    </ul>
    
    <!-- Summary -->
    <div class="summary">
        <h3>Operations Summary</h3>
        <div class="col-md-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th><i class="fa fa-calendar"></i> Bookings <a href="" class="pull-right">View All</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Pending <span class="pull-right">0</span></td>
                    </tr>
                    <tr>
                        <td>Approved <span class="pull-right">0</span></td>
                    </tr>
                    <tr>
                        <td>Declined <span class="pull-right">0</span></td>
                    </tr>
                    <tr>
                        <td>Done <span class="pull-right">0</span></td>
                    </tr>
                    <tr>
                        <td>Total <span class="pull-right">0</span></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th><i class="fa fa-calendar"></i> Shipping <a href="" class="pull-right">View All</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Pending <span class="pull-right">1</span></td>
                    </tr>
                    <tr>
                        <td>Active <span class="pull-right">1</span></td>
                    </tr>
                    <tr>
                        <td>Returned Shipments <span class="pull-right">4</span></td>
                    </tr>
                    <tr>
                        <td>Delivered <span class="pull-right">2</span></td>
                    </tr>
                    <tr>
                        <td>Declined <span class="pull-right">3</span></td>
                    </tr>
                    <tr>
                        <td>Total <span class="pull-right">0</span></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th><i class="fa fa-calendar"></i> Support <a href="" class="pull-right">View All</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>New <span class="pull-right">1</span></td>
                    </tr>
                    <tr>
                        <td>Active <span class="pull-right">1</span></td>
                    </tr>
                    <tr>
                        <td>Resolved <span class="pull-right">4</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection