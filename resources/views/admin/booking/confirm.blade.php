@extends('admin.layout')

@section('title', 'Booking')


@section('added-styles')
      <link rel="stylesheet" type="text/css" href="{!! asset('admin/css/dataTables.bootstrap.css') !!}">
@endsection

@section('added-scripts')
    
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Booking Confirmation</h1>
        </div>
    </div>
    <hr>
    
    <div class="clearfix"></div>



    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tbody>
                  <tr>
                    <td>Booking ID:</td>
                    <td>{!! $booking->id !!}</td>
                  </tr>
                  <tr>
                  <td>Customer Name:</td>
                  <td>{!! $booking->user->detail->firstname. ' ' . $booking->user->detail->middlename .  ' ' . $booking->user->detail->lastname !!}</td>
                  </td>
                  <tr>
                    <td>Contact Number:</td>
                    <td>{!! $booking->user->contact->contact_number !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Person Name:</td>
                    <td>{!! $booking->pickup_person_name !!}</td>
                  </tr>
                   <tr>
                    <td>Pickup Person Contact Number:</td>
                    <td>{!! $booking->pickup_contact_number !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Address 1:</td>
                    <td>{!! $booking->pickup_person_address_line_1 !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Address 2:</td>
                    <td>{!! $booking->pickup_person_address_line_2 !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Barangay:</td>
                    <td>{!! $booking->pickup_barangay !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup City:</td>
                    <td>{!! $booking->pickup_city !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup State / Province:</td>
                    <td>{!! $booking->pickup_state_province !!}</td>
                  </tr>
                  <tr>
                    <td>Pickup Country:</td>
                    <td>{!! $booking->pickup_country !!}</td>
                  </tr>

                </tbody>
            </table>
        </div>
        <br />
        <div class="col-md-6">
          <div class="row">
              <div class="col-md-6">
                  <a href="{!! route('admin-booking-index') !!}" class="btn btn-primary btn-block btn-lg">View Bookings</a>
              </div>
              <div class="col-md-6">
                  <a href="{!! route('admin-booking-create') !!}" class="btn btn-primary btn-block btn-lg">Add Another</a>
              </div>
          </div>
        </div>
    </div>
    <br>
    <br>
@endsection