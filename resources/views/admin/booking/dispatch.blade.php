@extends('admin.layout')

@section('title', 'for Dispatch Booking')


@section('added-styles')
      <link rel="stylesheet" type="text/css" href="{!! asset('admin/css/dataTables.bootstrap.css') !!}">
@endsection

@section('added-scripts')
    <script type="text/javascript" src="{!! asset('admin/js/jquery.dataTables.js') !!}"> </script>
    <script type="text/javascript" src="{!! asset('admin/js/dataTables.bootstrap.js') !!}"> </script>
    <script type="text/javascript" src="{!! asset('admin/js/dataTables.bootstrapPagination.js') !!}"> </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "sDom": "<'row'<'col-xs-12'T><'col-xs-12'f>r>t<'row'<'col-xs-12'i><'col-xs-12'p>>", 
                "sPaginationType": "bootstrap"
            } );




        $("#status").change(function (){ 
           $status =  $(this).val();

           if($status == 2) {

              $.get('../../riders', function(data){
                  var option = "";
                  for (var i = 0; i < data.length; i++) {
                      option += "<option value='" + data[i]['id'] +"'>";
                      option +=  data[i]['detail']['firstname'] + ' ' + data[i]['detail']['lastname'];
                      option += "</option>";
                  };
                 
                 //alert(option);
                  $("#riders").append(option);
              },'json');

           }

        });  

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });


        $("#riders").change(function (){ 
            var booking_id = $("#booking_id").val();
            var rider_id = $(this).val();
            $.ajax({
                method: "POST",
                url: "booking/set/rider",
                data: { booking_id: booking_id, rider_id: rider_id }
              })
                .done(function( data) {
                   if( data == "done" ) {
                      location.reload();
                   }
                });
        });
        } );


    </script>
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Booking</h1>
        </div>
        <div class="pull-right">
            <a href="{!! route('admin-booking-create') !!}"><button><i class="fa fa-plus"></i> Create New</button></a>
        </div>
        <div class="clearfix"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
    </div>
    <hr>

    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="datatable">
          <thead>
              <tr>
                  <th>Booking ID</th>
                  <th>Contact Person</th>
                  <th>Contact #</th>
                  <th>Status</th>
                  <th>Rider</th>
                  <th>City</th>
                  <th>Country</th>
                  <th>Pickup Address</th>
                  {{-- <th>Action</th> --}}
              </tr>
          </thead>
          <tbody>
              @foreach($bookings as $booking)
              <tr>
                  <input type="hidden" value="{!! $booking->id !!}" id="booking_id"/>
                  <td>{!!$booking->id!!}</td>
                  <td>{!! $booking->pickup_person_name !!}</td>
                  <td>{!! $booking->pickup_contact_number !!}</td>
                  <td>
                    <select id="status">
                          <option value="">Update Status</option>
                          {{-- <option value="1">Pending</option> --}}
                          {{-- <option value="2">Assign to rider</option> --}}
                          <option value="3">Dispatch</option>
                          <option value="4">Cancel</option>
                      </select> 
                  </td>
                  <td>
                      <select id="riders">
                          <option value="">Select Rider</option>
                          
                      </select> 
                  </td>
                  <td>{{$booking->pickup_city}}</td>
                  <td>{{$booking->pickup_country}}</td>
                  <td>{!! $booking->pickup_person_address_line_1 . ' ' .  $booking->pickup_person_address_line_2 . ' ' . $booking->pickup_barangay . ' ' . $booking->pickup_city . ' ' . $booking->pickup_state_province . ' ' . $booking->pickup_country !!}</td>
                  {{-- <td>Actions</td> --}}
              </tr>
              @endforeach
        </tbody>
    </table>


@endsection