@extends('admin.layout')

@section('title', 'Booking')


@section('added-styles')
      <link rel="stylesheet" type="text/css" href="{!! asset('admin/css/dataTables.bootstrap.css') !!}">
@endsection

@section('added-scripts')
    <script type="text/javascript" src="{!! asset('admin/js/jquery.js') !!}"> </script>
    <script type="text/javascript" src="{!! asset('admin/js/jquery.dataTables.js') !!}"> </script>
    <script type="text/javascript" src="{!! asset('admin/js/dataTables.bootstrap.js') !!}"> </script>
    <script type="text/javascript" src="{!! asset('admin/js/dataTables.bootstrapPagination.js') !!}"> </script>

    <script type="text/javascript"  src="{!! asset('admin/js/typehead.js') !!}"></script>

    <script src="{!! asset('admin/js/main.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable( {
                "sDom": "<'row'<'col-xs-12'T><'col-xs-12'f>r>t<'row'<'col-xs-12'i><'col-xs-12'p>>",
                "sPaginationType": "bootstrap"
            } );
        } );



        
    </script>
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Booking</h1>
        </div>
        <div class="pull-right">
            <a href="{!! route('admin-booking-create') !!}"><button><i class="fa fa-plus"></i> Create New</button></a>
        </div>
        <div class="clearfix"></div>
        <p>List of Old Customer available to add booking</p>
    </div>
    <hr>

    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="datatable">
          <thead>
              <tr>
                  <th>Customer Name</th>
                  <th>Contact #</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              @foreach($users as $user)
              <tr>
                  <td>{!! $user->detail->firstname . ' ' . $user->detail->middlename . ' ' . $user->detail->lastname  !!}</td>
                  <td>{!! $user->contact->contact_number !!}</td>                  
                  <td><a href="{!! route('admin-booking-create-old-detail', $user->id)!!}">Add Booking</a></td>
              </tr>
              @endforeach
        </tbody>
    </table>


@endsection