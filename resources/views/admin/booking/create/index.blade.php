@extends('admin.layout')

@section('title', 'Booking')


@section('added-styles')
      
@endsection

@section('added-scripts')
    
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Create Booking</h1>
        </div>
        <div class="pull-right">
            <a href="{!! route('admin-booking-index') !!}"><button><i class="fa fa-mail-reply"></i> Back to Bookings</button></a>
        </div>
        <div class="clearfix"></div>
        <p>Add new bookings for new and old customer. </p>
    </div>
    <hr>
    <div class="content-head">
        <div class="pull-left">
            <h2>Step 01</h2>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-6">
          <div class="row">
              <div class="col-md-6">
                  <a href="{!! route('admin-booking-create-old') !!}" class="btn btn-primary btn-block btn-lg">Old Customer</a>
              </div>
              <div class="col-md-6">
                  <a href="{!! route('admin-booking-create-new') !!}" class="btn btn-primary btn-block btn-lg">New Customer</a>
              </div>
          </div>
        </div>
    </div>

@endsection