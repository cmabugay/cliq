@extends('admin.layout')

@section('title', 'Booking')


@section('added-styles')
      <link rel="stylesheet" type="text/css" href="{!! asset('admin/css/dataTables.bootstrap.css') !!}">

@endsection

@section('added-scripts')
	<script type="text/javascript"  src="{!! asset('admin/js/typehead.js') !!}"></script>
    <script type="text/javascript">
    	$('.next').click(function(){

  var nextId = $(this).parents('.tab-pane').next().attr("id");
					  $('[href=#'+nextId+']').tab('show');

					})

					$('.first').click(function(){

					  $('#myWizard a:first').tab('show')

					})

		$.get('../../../provinces', function(data){
		    $("#region_province").typeahead({ source:data });
		},'json');

		$.get('../../../city', function(data){
		    $("#city_municipality").typeahead({ source:data });
		},'json');

		$.get('../../../country', function(data){

		    $("#country").typeahead({ source:data });
		},'json');

    </script>
@endsection


@section('content')
    <div class="dashboard-head">
        <div class="pull-left">
            <h1>Create Booking</h1>
        </div>
        <div class="pull-right">
            <a href="{!! route('admin-booking-index') !!}"><button><i class="fa fa-mail-reply"></i> Back to Bookings</button></a>
        </div>
        <div class="clearfix"></div>
        <p>Add booking details for the new customer.</p>
    </div>
    <hr>
    <div class="clearfix"></div>
    <div class="col-md-12" id="myWizard">
	   <div class="navbar">
	      <div class="navbar-inner">
	            <ul class="nav nav-pills">
	               <li class="active"><a href="#step1" data-toggle="tab">User Information</a></li>
	               <li><a href="#step2" data-toggle="tab">Booking Details</a></li>
	      </div>
	   </div>
    	<form class="col-md-8" method="POST" action="{!! route('admin-booking-store') !!}">
  		{!! csrf_field() !!}
  		<input type="hidden" name="customer_type" value="new">
	    <div class="tab-content">
	     <div class="tab-pane active" id="step1">
	         	 <div class="row">
			        <div class="col-md-12">
			        	<h3>User Details</h3>
						  <div class="form-group col-md-4 {!! $errors->first('firstname') ? 'has-error' : '' !!}">
						    <label for="firstname"> Firstname <span class="required">*</span></label>
						    <input type="text" class="form-control" id="firstname"  name="firstname" placeholder="" value="{!! old('firstname') !!}">
						  	@if( $errors->first('firstname') )
						  		{!!  $errors->first('firstname') !!}
						  	@endif
						  </div>
						  <div class="form-group col-md-4 {!! $errors->first('middlename') ? 'has-error' : '' !!}">
						    <label for="middlename"> Middlename </label>
						    <input type="text" class="form-control" id="middlename"  name="middlename" placeholder="" value="{!! old('middlename') !!}">
						  	@if( $errors->first('middlename') )
						  		{!!  $errors->first('middlename') !!}
						  	@endif
						  </div>
						  <div class="form-group col-md-4 {!! $errors->first('lastname') ? 'has-error' : '' !!}">
						    <label for="lastname"> Lastname <span class="required">*</span></label>
						    <input type="text" class="form-control" id="lastname"  name="lastname" placeholder="" value="{!! old('lastname') !!}">
						  	@if( $errors->first('lastname') )
						  		{!!  $errors->first('lastname') !!}
						  	@endif
						  </div>
						  <div class="form-group col-md-12 {!! $errors->first('contact_number') ? 'has-error' : '' !!}">
						    <label for="contact_number"> Contact Number <span class="required">*</span></label>
						    <input type="text" class="form-control" id="contact_number"  name="contact_number" placeholder="" value="{!! old('contact_number') !!}">
						  	@if( $errors->first('contact_number') )
						  		{!!  $errors->first('contact_number') !!}
						  	@endif
						  </div>
						  
						  <div class="form-group col-md-12 {!! $errors->first('email') ? 'has-error' : '' !!}">
						    <label for="email">Email / Username </label>
						    <input type="text" class="form-control" id="email"  name="email" placeholder="you@site.com" value="{!! old('email') !!}">
						  	@if( $errors->first('email') )
						  		<p class="help-block">{!!  $errors->first('email') !!}</p>
						  	@endif
						  </div>
						  <!--
						  <div class="form-group col-md-6 {!! $errors->first('password') ? 'has-error' : '' !!}">
						    <label for="street_address_1">Password <span class="required">*</span></label>
						    <input type="password" class="form-control" id="password" name="password" value="{!! old('password') !!}">
						  	@if( $errors->first('password') )
						  		<p class="help-block">{!!  $errors->first('password') !!}</p>
						  	@endif
						  </div>

						  <div class="form-group col-md-6  {!! $errors->first('confirm_password') ? 'has-error' : '' !!}">
						    <label for="confirm_password">Confirm Password <span class="required">*</span></label>
						    <input type="password" class="form-control" id="confirm_password" name="confirm_password"  value="{!! old('confirm_password') !!}">
						  	@if( $errors->first('confirm_password') )
						  		<p class="help-block">{!!  $errors->first('confirm_password') !!}</p>
						  	@endif
						  </div>
						  -->
						
			        </div> 	
			    </div>
	         <a class="btn btn-primary next" href="#">Continue</a>
	      </div>
	      <div class="tab-pane" id="step2">
	         <div class="form-group col-md-6 {!! $errors->first('contact_person_name') ? 'has-error' : '' !!}">
			    <label for="contact_person_name">Contact Person Name <span class="required">*</span></label>
			    <input type="text" class="form-control" id="contact_person_name"  name="contact_person_name" placeholder="User" value="{!! old('contact_person_name') !!}">
			  	@if( $errors->first('contact_person_name') )
			  		{!!  $errors->first('contact_person_name') !!}
			  	@endif
			  </div>
			  <div class="form-group col-md-6 {!! $errors->first('booking_contact_number') ? 'has-error' : '' !!}">
			    <label for="booking_contact_number">Contact Number</label>
			    <input type="text" class="form-control" id="booking_contact_number" name="booking_contact_number" placeholder="+63 9XX XXX XXXX" value="{!! old('booking_contact_number') !!}">
			 	@if( $errors->first('booking_contact_number') )
			  		<p class="help-block">{!!  $errors->first('booking_contact_number') !!}</p>
			  	@endif
			  </div>
			  <h4>Contact Address</h4>
			  <div class="form-group col-md-6 {!! $errors->first('contact_person_name') ? 'has-error' : '' !!}">
			    <label for="region_province">Region / Province <span class="required">*</span></label>
			    <input type="text" class="form-control" id="region_province"  name="region_province" placeholder="State" value="{!! old('contact_number') !!}">
			  	@if( $errors->first('region_province') )
			  		<p class="help-block">{!!  $errors->first('region_province') !!}</p>
			  	@endif
			  </div>
			  <div class="form-group col-md-6 {!! $errors->first('street_address_1') ? 'has-error' : '' !!}">
			    <label for="street_address_1">Steet Address 1 <span class="required">*</span></label>
			    <input type="text" class="form-control" id="street_address_1" name="street_address_1" placeholder="Address" value="{!! old('street_address_1') !!}">
			  	@if( $errors->first('street_address_1') )
			  		<p class="help-block">{!!  $errors->first('street_address_1') !!}</p>
			  	@endif
			  </div>

			  <div class="form-group col-md-6  {!! $errors->first('city_municipality') ? 'has-error' : '' !!}">
			    <label for="city_municipality">City / Municipality <span class="required">*</span></label>
			    <input type="text" class="form-control" id="city_municipality" name="city_municipality" placeholder="City" value="{!! old('city_municipality') !!}">
			  	@if( $errors->first('city_municipality') )
			  		<p class="help-block">{!!  $errors->first('city_municipality') !!}</p>
			  	@endif
			  </div>
			  <div class="form-group col-md-6 {!! $errors->first('street_address_2') ? 'has-error' : '' !!}">
			    <label for="street_address_2">Steet Address 2</label>
			    <input type="text" class="form-control" id="street_address_2" name="street_address_2" placeholder="Address" value="{!! old('street_address_2') !!}">
			  	@if( $errors->first('street_address_2') )
			  		<p class="help-block">{!!  $errors->first('street_address_2') !!}</p>
			  	@endif
			  </div>
			  <div class="form-group col-md-6 {!! $errors->first('barangay') ? 'has-error' : '' !!}">
			    <label for="barangay">Barangay </label>
			    <input type="text" class="form-control" id="barangay" name="barangay" placeholder="Barangay" value="{!! old('barangay') !!}">
			  	@if( $errors->first('barangay') )
			  		<p class="help-block">{!!  $errors->first('barangay') !!}</p>
			  	@endif
			  </div>
			  <div class="form-group col-md-6 {!! $errors->first('zip_code') ? 'has-error' : '' !!}">
			    <label for="zip_code">Zip Code <span class="required">*</span></label>
			    <input type="text" class="form-control" id="zip_code" name="zip code" placeholder="zip_code" value="{!! old('zip_code') !!}">
			  	@if( $errors->first('zip_code') )
			  		<p class="help-block">{!!  $errors->first('zip_code') !!}</p>
			  	@endif
			  </div>
			   <div class="form-group col-md-12 {!! $errors->first('country') ? 'has-error' : '' !!}">
			    <label for="country">Country <span class="required">*</span></label>
			    <input type="text" class="form-control" id="country" name="country" placeholder="Philippines" value="{!! old('country') !!}">
			  	@if( $errors->first('zip_code') )
			  		<p class="help-block">{!!  $errors->first('country') !!}</p>
			  	@endif
			  </div>
			  <div class="form-group col-md-12">
			    <label for="remarks">Remarks </label>
			    <textarea class="form-control" id="remarks" name="remarks">Add your message here...</textarea>
			    @if( $errors->first('remarks') )
			  		<p class="help-block">{!!  $errors->first('remarks') !!}</p>
			  	@endif
			  </div>
	         <button type="submit" class="btn btn-primary">Create Booking</button>
	      </div>
	     
	    </div>
		</form>
	</div>
@endsection