<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cliq</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="{!! asset('admin/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('admin/css/bootstrap.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('admin/css/bootstrap-theme.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('admin/css/jquery.bxslider.css') !!}">
        <link rel="stylesheet" href="{!! asset('admin/css/main.css?v=1.1') !!}">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>

        <div class="login">
            <div class="login-ban">
                 <img src="{!! asset('admin/img/login-header.jpg') !!}" alt="">
                 <h2>USER LOGIN</h2>
            </div>
           

            <div class="login-form">
                <form method="POST" action="{!! url('login') !!}">
                    {!! csrf_field() !!}
                    @if( Session::has('message') )
                            <p class="alert alert-danger" role="alert">{!!  Session::get('message')  !!}
                    @endif
                    <div class="form-group {!! $errors->first('username') ? 'has-error' : '' !!}">
                        <input type="text" class="form-control" placeholder="USERNAME" name="username" value="{!! old('username') !!}">
                        @if( $errors->first('username') )
                            {!!  $errors->first('username') !!}
                        @endif
                    </div>
                    <div class="form-group {!! $errors->first('password') ? 'has-error' : '' !!}">
                        <input type="password" class="form-control" placeholder="PASSWORD" name="password">
                        @if( $errors->first('password') )
                            {!!  $errors->first('password') !!}
                        @endif
                    </div>
                    <div class="col-md-6 remember">
                         <label>
                          <input type="checkbox"> REMEMBER ME
                        </label>
                    </div>
                    <div class="col-md-6 forgot">
                        <a href="" >FORGOT PASSWORD</a>
                    </div>
                    <div class="clearfix"></div>
                    <button type="submit">SIGN IN</button>
                </form>
                <hr>
                <a href="" class="login-social"><i class="fa fa-facebook-square"></i> <span>Login Using <br> Facebook Account</span></a>
                <a href="" class="login-social"><i class="fa fa-user-plus"></i> <span>Don't have an account <br> Sign up</span></a>
                <div class="clearfix"></div>
            </div>
        </div>

        <script src="{!! ('admin/js/vendor/jquery-1.11.2.min.js') !!}"></script>

        <script src="{!! ('admin/js/vendor/bootstrap.min.js') !!}"></script>

        <script src="{!! ('admin/js/jquery.bxslider.js') !!}"></script>

        <script src="{!! ('admin/js/main.js') !!}"></script>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            // (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            // function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            // e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            // e.src='//www.google-analytics.com/analytics.js';
            // r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            // ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
